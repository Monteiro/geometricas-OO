//
//  geometrica.h
//  geometricas
//
//  Created by Franck Monteiro Santana on 26/03/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

//ESPECIFICANDO A CLASSE
//HEADERS - CABECALHO

#ifndef __geometricas__geometrica__
#define __geometricas__geometrica__

class Geometrica{
    //atributos
    private:
    float base, altura;
    //header dos construtores
    public:
    //tem uma sobrecarga de contrutor
        Geometrica();
        Geometrica(float base, float altura);
    //acessadores
        float getBase();
        float getAltura();
        void setBase(float base);
        void setAltura(float altura);
    //na classe geometrica vc nao vai implementar mas os filhos vao ser obrigados a implementar
        virtual float area() = 0;
    
};

#endif