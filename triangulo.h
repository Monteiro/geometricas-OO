//
//  triangulo.h
//  geometricas
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#ifndef __geometricas__triangulo__
#define __geometricas__triangulo__

#include "geometrica.h"

class Triangulo : public Geometrica
{
public:
    Triangulo();
    Triangulo(float altura, float base);
    float area();
    float area(float altura, float base);
};

#endif /* defined(__geometricas__triangulo__) */
