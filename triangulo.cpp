//
//  triangulo.cpp
//  geometricas
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include "triangulo.h"

Triangulo::Triangulo()
{
    setBase(15);
    setAltura(15);
}

Triangulo::Triangulo(float altura, float base)
{
    setBase(base);
    setAltura(altura);
}

float Triangulo::area()
{
    return getAltura() * getBase() / 2;
}

float Triangulo::area(float altura, float base)
{
    return altura * base / 2;
}

