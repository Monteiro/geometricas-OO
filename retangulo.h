//
//  retangulo.h
//  geometricas
//
//  Created by Franck Monteiro Santana on 26/03/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#ifndef __geometricas__retangulo__
#define __geometricas__retangulo__

#include "geometrica.h"

//a classe retangulo vai receber os atributos de geometrica
//o filho acessa os atributos do pai atraves dos metodos

class Retangulo : public Geometrica
{
    //atributos
    public:
        Retangulo();
        Retangulo(float base, float altura);
    //header ou cabecalho do(s) construtor(es)/metodos
        float area();
        float area(float base, float altura);
    
};

#endif
