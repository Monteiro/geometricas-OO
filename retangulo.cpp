//
//  retangulo.cpp
//  geometricas
//
//  Created by Franck Monteiro Santana on 26/03/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include "retangulo.h"

retangulo::retangulo()
{
    setBase(10);
    setAltura(20);
}

Retangulo::Retangulo(float base, float altura)
{
    setBase(base);
    setAltura(altura);
}

float Retangulo::area()
{
    return getBase() * getAltura();
}

/*float retangulo::area(float base, float altura)
{
    return base * altura;
}*/

//PODERIA TER SIDO ASSIM, com esse outro metodo