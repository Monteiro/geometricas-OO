//
//  principal.cpp
//  geometricas
//
//  Created by Franck Monteiro Santana on 26/03/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//
#include "triangulo.h"
#include "retangulo.h"
#include "geometrica.h"
#include <iostream>

using namespace std;

int main()
{
    Geometrica *formaGeometrica;
    formaGeometrica = new Retangulo(20,20);
    cout << "Resultado da Area do Retangulo: " << formaGeometrica->area() << endl;
    Triangulo *a;
    a = new Triangulo (25, 25);
    cout << "Resultado da Area do Triangulo: " << a->area() << endl;
    
    
}