//
//  geometrica.cpp
//  geometricas
//
//  Created by Franck Monteiro Santana on 26/03/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include "geometrica.h"

//construtor vazio
//metodos
geometrica::geometrica()
{
    setBase(10);
    setAltura(10);
}
//construtor com dois parametros
geometrica::geometrica(float base, float altura)
{
    setBase(base);
    setAltura(altura);
}

//encapsulamento por isso que eu to usando o get e set(que sao os metodos/funcoes)

float geometrica::getBase()
{
    return base;
}
float geometrica::getAltura()
{
    return altura;
}

void geometrica::setBase(float base)
{
    this->base = base;
}
void geometrica::setAltura(float altura)
{
    this->altura = altura;
}

//NOME DA CLASSE::NOME DO METODO(CONSTRUTOR OU ACESSADOR)

//this-> o atributo de tal classe vai receber o parametro.